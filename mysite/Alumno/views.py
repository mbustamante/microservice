from django.shortcuts import render
from models import Alumno
from rest_framework import viewsets
from serializers import AlumnoSerializer


class AlumnoViewSet(viewsets.ModelViewSet):
    queryset = Alumno.objects.all()
    serializer_class = AlumnoSerializer
