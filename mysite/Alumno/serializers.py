from models import Alumno
from rest_framework import serializers


class AlumnoSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Alumno
		fields = ('name','phone','address','email')