from django.db import models

class Alumno(models.Model):
	name = models.CharField(max_length=30)
	phone = models.CharField(max_length=12)
	address=models.CharField(max_length=60)
	email = models.CharField(max_length=40)
	class Meta:
		db_table = "alumnos"
